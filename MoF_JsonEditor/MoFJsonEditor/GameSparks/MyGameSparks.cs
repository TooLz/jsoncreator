using System;
using System.Collections.Generic;
using GameSparks.Core;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!

namespace GameSparks.Api.Requests{
	public class LogEventRequest_CleanUpTeamData : GSTypedRequest<LogEventRequest_CleanUpTeamData, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_CleanUpTeamData() : base("LogEventRequest"){
			request.AddString("eventKey", "CleanUpTeamData");
		}
		
		public LogEventRequest_CleanUpTeamData Set_teamName( string value )
		{
			request.AddString("teamName", value);
			return this;
		}
	}
	
	public class LogChallengeEventRequest_CleanUpTeamData : GSTypedRequest<LogChallengeEventRequest_CleanUpTeamData, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_CleanUpTeamData() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "CleanUpTeamData");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_CleanUpTeamData SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_CleanUpTeamData Set_teamName( string value )
		{
			request.AddString("teamName", value);
			return this;
		}
	}
	
	public class LogEventRequest_GenerateDungeon : GSTypedRequest<LogEventRequest_GenerateDungeon, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_GenerateDungeon() : base("LogEventRequest"){
			request.AddString("eventKey", "GenerateDungeon");
		}
	}
	
	public class LogChallengeEventRequest_GenerateDungeon : GSTypedRequest<LogChallengeEventRequest_GenerateDungeon, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_GenerateDungeon() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "GenerateDungeon");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_GenerateDungeon SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
	}
	
	public class LogEventRequest_GetDungeon : GSTypedRequest<LogEventRequest_GetDungeon, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_GetDungeon() : base("LogEventRequest"){
			request.AddString("eventKey", "GetDungeon");
		}
	}
	
	public class LogChallengeEventRequest_GetDungeon : GSTypedRequest<LogChallengeEventRequest_GetDungeon, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_GetDungeon() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "GetDungeon");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_GetDungeon SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
	}
	
	public class LogEventRequest_GetDungeonInfo : GSTypedRequest<LogEventRequest_GetDungeonInfo, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_GetDungeonInfo() : base("LogEventRequest"){
			request.AddString("eventKey", "GetDungeonInfo");
		}
	}
	
	public class LogChallengeEventRequest_GetDungeonInfo : GSTypedRequest<LogChallengeEventRequest_GetDungeonInfo, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_GetDungeonInfo() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "GetDungeonInfo");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_GetDungeonInfo SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
	}
	
	public class LogEventRequest_GetDungeonRoomInfo : GSTypedRequest<LogEventRequest_GetDungeonRoomInfo, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_GetDungeonRoomInfo() : base("LogEventRequest"){
			request.AddString("eventKey", "GetDungeonRoomInfo");
		}
		public LogEventRequest_GetDungeonRoomInfo Set_RoomNumber( long value )
		{
			request.AddNumber("RoomNumber", value);
			return this;
		}			
	}
	
	public class LogChallengeEventRequest_GetDungeonRoomInfo : GSTypedRequest<LogChallengeEventRequest_GetDungeonRoomInfo, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_GetDungeonRoomInfo() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "GetDungeonRoomInfo");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_GetDungeonRoomInfo SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_GetDungeonRoomInfo Set_RoomNumber( long value )
		{
			request.AddNumber("RoomNumber", value);
			return this;
		}			
	}
	
	public class LogEventRequest_GetUserData : GSTypedRequest<LogEventRequest_GetUserData, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_GetUserData() : base("LogEventRequest"){
			request.AddString("eventKey", "GetUserData");
		}
	}
	
	public class LogChallengeEventRequest_GetUserData : GSTypedRequest<LogChallengeEventRequest_GetUserData, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_GetUserData() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "GetUserData");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_GetUserData SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
	}
	
	public class LogEventRequest_GetVersionNumber : GSTypedRequest<LogEventRequest_GetVersionNumber, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_GetVersionNumber() : base("LogEventRequest"){
			request.AddString("eventKey", "GetVersionNumber");
		}
	}
	
	public class LogChallengeEventRequest_GetVersionNumber : GSTypedRequest<LogChallengeEventRequest_GetVersionNumber, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_GetVersionNumber() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "GetVersionNumber");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_GetVersionNumber SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
	}
	
	public class LogEventRequest_JoinPartyByName : GSTypedRequest<LogEventRequest_JoinPartyByName, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_JoinPartyByName() : base("LogEventRequest"){
			request.AddString("eventKey", "JoinPartyByName");
		}
		
		public LogEventRequest_JoinPartyByName Set_PartyName( string value )
		{
			request.AddString("PartyName", value);
			return this;
		}
	}
	
	public class LogChallengeEventRequest_JoinPartyByName : GSTypedRequest<LogChallengeEventRequest_JoinPartyByName, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_JoinPartyByName() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "JoinPartyByName");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_JoinPartyByName SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_JoinPartyByName Set_PartyName( string value )
		{
			request.AddString("PartyName", value);
			return this;
		}
	}
	
	public class LogEventRequest_UpdateCharacterData : GSTypedRequest<LogEventRequest_UpdateCharacterData, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_UpdateCharacterData() : base("LogEventRequest"){
			request.AddString("eventKey", "UpdateCharacterData");
		}
		public LogEventRequest_UpdateCharacterData Set_CharData( GSData value )
		{
			request.AddObject("CharData", value);
			return this;
		}			
	}
	
	public class LogChallengeEventRequest_UpdateCharacterData : GSTypedRequest<LogChallengeEventRequest_UpdateCharacterData, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_UpdateCharacterData() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "UpdateCharacterData");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_UpdateCharacterData SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_UpdateCharacterData Set_CharData( GSData value )
		{
			request.AddObject("CharData", value);
			return this;
		}
		
	}
	
	public class LogEventRequest_UpdateCombatStats : GSTypedRequest<LogEventRequest_UpdateCombatStats, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_UpdateCombatStats() : base("LogEventRequest"){
			request.AddString("eventKey", "UpdateCombatStats");
		}
		public LogEventRequest_UpdateCombatStats Set_UserInfo( GSData value )
		{
			request.AddObject("UserInfo", value);
			return this;
		}			
		public LogEventRequest_UpdateCombatStats Set_MonsterInfo( GSData value )
		{
			request.AddObject("MonsterInfo", value);
			return this;
		}			
		public LogEventRequest_UpdateCombatStats Set_RoomNum( long value )
		{
			request.AddNumber("RoomNum", value);
			return this;
		}			
	}
	
	public class LogChallengeEventRequest_UpdateCombatStats : GSTypedRequest<LogChallengeEventRequest_UpdateCombatStats, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_UpdateCombatStats() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "UpdateCombatStats");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_UpdateCombatStats SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_UpdateCombatStats Set_UserInfo( GSData value )
		{
			request.AddObject("UserInfo", value);
			return this;
		}
		
		public LogChallengeEventRequest_UpdateCombatStats Set_MonsterInfo( GSData value )
		{
			request.AddObject("MonsterInfo", value);
			return this;
		}
		
		public LogChallengeEventRequest_UpdateCombatStats Set_RoomNum( long value )
		{
			request.AddNumber("RoomNum", value);
			return this;
		}			
	}
	
	public class LogEventRequest_UpdateDungeonRoom : GSTypedRequest<LogEventRequest_UpdateDungeonRoom, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_UpdateDungeonRoom() : base("LogEventRequest"){
			request.AddString("eventKey", "UpdateDungeonRoom");
		}
		public LogEventRequest_UpdateDungeonRoom Set_RoomContents( GSData value )
		{
			request.AddObject("RoomContents", value);
			return this;
		}			
	}
	
	public class LogChallengeEventRequest_UpdateDungeonRoom : GSTypedRequest<LogChallengeEventRequest_UpdateDungeonRoom, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_UpdateDungeonRoom() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "UpdateDungeonRoom");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_UpdateDungeonRoom SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_UpdateDungeonRoom Set_RoomContents( GSData value )
		{
			request.AddObject("RoomContents", value);
			return this;
		}
		
	}
	
	public class LogEventRequest_UpdateDungeonRoomInfo : GSTypedRequest<LogEventRequest_UpdateDungeonRoomInfo, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_UpdateDungeonRoomInfo() : base("LogEventRequest"){
			request.AddString("eventKey", "UpdateDungeonRoomInfo");
		}
		
		public LogEventRequest_UpdateDungeonRoomInfo Set_DungeonRoomInfo( string value )
		{
			request.AddString("DungeonRoomInfo", value);
			return this;
		}
		public LogEventRequest_UpdateDungeonRoomInfo Set_RoomNumber( long value )
		{
			request.AddNumber("RoomNumber", value);
			return this;
		}			
	}
	
	public class LogChallengeEventRequest_UpdateDungeonRoomInfo : GSTypedRequest<LogChallengeEventRequest_UpdateDungeonRoomInfo, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_UpdateDungeonRoomInfo() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "UpdateDungeonRoomInfo");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_UpdateDungeonRoomInfo SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_UpdateDungeonRoomInfo Set_DungeonRoomInfo( string value )
		{
			request.AddString("DungeonRoomInfo", value);
			return this;
		}
		public LogChallengeEventRequest_UpdateDungeonRoomInfo Set_RoomNumber( long value )
		{
			request.AddNumber("RoomNumber", value);
			return this;
		}			
	}
	
	public class LogEventRequest_UpdateInventory : GSTypedRequest<LogEventRequest_UpdateInventory, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_UpdateInventory() : base("LogEventRequest"){
			request.AddString("eventKey", "UpdateInventory");
		}
		public LogEventRequest_UpdateInventory Set_Inventory( GSData value )
		{
			request.AddObject("Inventory", value);
			return this;
		}			
	}
	
	public class LogChallengeEventRequest_UpdateInventory : GSTypedRequest<LogChallengeEventRequest_UpdateInventory, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_UpdateInventory() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "UpdateInventory");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_UpdateInventory SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_UpdateInventory Set_Inventory( GSData value )
		{
			request.AddObject("Inventory", value);
			return this;
		}
		
	}
	
	public class LogEventRequest_UpdateMonsterDB : GSTypedRequest<LogEventRequest_UpdateMonsterDB, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_UpdateMonsterDB() : base("LogEventRequest"){
			request.AddString("eventKey", "UpdateMonsterDB");
		}
		public LogEventRequest_UpdateMonsterDB Set_Monsters( GSData value )
		{
			request.AddObject("Monsters", value);
			return this;
		}			
	}
	
	public class LogChallengeEventRequest_UpdateMonsterDB : GSTypedRequest<LogChallengeEventRequest_UpdateMonsterDB, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_UpdateMonsterDB() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "UpdateMonsterDB");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_UpdateMonsterDB SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_UpdateMonsterDB Set_Monsters( GSData value )
		{
			request.AddObject("Monsters", value);
			return this;
		}
		
	}
	
	public class LogEventRequest_UpdateTracker : GSTypedRequest<LogEventRequest_UpdateTracker, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_UpdateTracker() : base("LogEventRequest"){
			request.AddString("eventKey", "UpdateTracker");
		}
		public LogEventRequest_UpdateTracker Set_TrackerData( GSData value )
		{
			request.AddObject("TrackerData", value);
			return this;
		}			
	}
	
	public class LogChallengeEventRequest_UpdateTracker : GSTypedRequest<LogChallengeEventRequest_UpdateTracker, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_UpdateTracker() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "UpdateTracker");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_UpdateTracker SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_UpdateTracker Set_TrackerData( GSData value )
		{
			request.AddObject("TrackerData", value);
			return this;
		}
		
	}
	
	public class LogEventRequest_UseEnergy : GSTypedRequest<LogEventRequest_UseEnergy, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_UseEnergy() : base("LogEventRequest"){
			request.AddString("eventKey", "UseEnergy");
		}
		public LogEventRequest_UseEnergy Set_EnergyToUse( long value )
		{
			request.AddNumber("EnergyToUse", value);
			return this;
		}			
	}
	
	public class LogChallengeEventRequest_UseEnergy : GSTypedRequest<LogChallengeEventRequest_UseEnergy, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_UseEnergy() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "UseEnergy");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_UseEnergy SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_UseEnergy Set_EnergyToUse( long value )
		{
			request.AddNumber("EnergyToUse", value);
			return this;
		}			
	}
	
	public class LogEventRequest_UserClaimRoom : GSTypedRequest<LogEventRequest_UserClaimRoom, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_UserClaimRoom() : base("LogEventRequest"){
			request.AddString("eventKey", "UserClaimRoom");
		}
		public LogEventRequest_UserClaimRoom Set_RoomNumber( long value )
		{
			request.AddNumber("RoomNumber", value);
			return this;
		}			
	}
	
	public class LogChallengeEventRequest_UserClaimRoom : GSTypedRequest<LogChallengeEventRequest_UserClaimRoom, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_UserClaimRoom() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "UserClaimRoom");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_UserClaimRoom SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_UserClaimRoom Set_RoomNumber( long value )
		{
			request.AddNumber("RoomNumber", value);
			return this;
		}			
	}
	
}
	

namespace GameSparks.Api.Messages {


}
