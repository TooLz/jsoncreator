namespace GameSparks
{
    /// <summary>
    /// This is the starting point for GameSparks in your Unity game. 
    /// Add this class to a GameObject to get started. 
    /// </summary>
    public class GameSparksUnity
    {
        /// <summary>
        /// You can override which connection settings GameSparks uses to connect to the backend with this member. 
        /// </summary>
        public GameSparksSettings Settings;

        void Start()
        {
        }
    }
}
